<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Http\Controllers\Controller;
use App\Http\Resources\Perpustakaan\BookCollection;
use App\Http\Resources\Perpustakaan\BookResource;
use App\Models\Perpustakaan\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $books = new BookCollection(Book::paginate());

        return $books;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rules = [
            'isbn' => ['required','numeric','unique:App\Models\Perpustakaan\Book,isbn'],
            'judul' => 'required',
            'harga' => 'required|numeric',
            'stock' => 'required',
            'tahun' => 'required',
        ];

        $errorMessages = [
            'isbn.required' => "isbn wajib diisi!",
            'isbn.unique' => "no isbn ". $request->isbn. " sudah terdaftar!",
            'title.required' => "title wajib diisi",
            'price.required' => "price wajib diisi",
            'stock.required' => "stock wajib diisi",
            'tahun.required' => "tahun wajib diisi",
        ];

        Validator::make($request->all(), $rules, $errorMessages)->validate();
        
        try {
            
            DB::beginTransaction();
            
            $book = Book::create([
                'isbn' => $request->isbn,
                'title' => $request->judul,
                'price' => $request->harga,
                'stock' => $request->stock,
                'year' => $request->tahun,
            ]);

            DB::commit();

            return response()->json($book->title . " berhasil disimpan");

        } catch (\Exception $e) {
           
            DB::rollBack();
           
           return response($e->getMessage(), 500);

        }

    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        return new BookResource($book);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Book $book)
    {
        $rules = [
            'isbn' => ['required','numeric', 
                Rule::unique('perpustakaan.books')->ignore($book)],
            'judul' => 'required',
            'harga' => 'required|numeric',
            'stock' => 'required',
            'tahun' => 'required',
        ];

        $errorMessages = [
            'isbn.required' => "isbn wajib diisi!",
            'isbn.unique' => "no isbn ". $request->isbn. " sudah terdaftar!",
            'title.required' => "title wajib diisi",
            'price.required' => "price wajib diisi",
            'stock.required' => "stock wajib diisi",
            'tahun.required' => "tahun wajib diisi",
        ];

        Validator::make($request->all(), $rules, $errorMessages)->validate();

        try {
            
            DB::beginTransaction();
            
            $book->update([
                'isbn' => $request->isbn,
                'title' => $request->judul,
                'price' => $request->harga,
                'stock' => $request->stock,
                'year' => $request->tahun,
            ]);

            DB::commit();

            return response()->json($book->title . " berhasil diubah");

        } catch (\Exception $e) {
           
            DB::rollBack();
           
           return response($e->getMessage(), 500);

        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return response()->json("buku berhasil dihapus");
    }
}
