<?php

namespace App\Http\Resources\Perpustakaan;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'judul' => $this->title,
            'no isbn' => $this->isbn,
            'tahun terbit' => $this->year,
            'pengarang' => $this->writer?->name,
            'penerbit' => $this->publisher?->name,
        ];
    }
}
