<?php

namespace App\Models\Perpustakaan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $connection = 'perpustakaan';

    protected $fillable = ['isbn', 'title', 'stock', 'price', 'year', 'publisher_id', 'writer_id'];

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    public function writer()
    {
        return $this->belongsTo(Writer::class);
    }

}
